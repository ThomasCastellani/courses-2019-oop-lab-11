﻿using System;

namespace ComplexNumbers
{
    class ComplexNum
    {
        public ComplexNum(double re, double im)
        {
            Re = re;
            Im = im;
        }

        public double Re { get; private set; }
        public double Im { get; private set; }


        // Restituisce il modulo del numero complesso
        public double Module
        {
            get
            {
                return Math.Sqrt((Re * Re) + (Im * Im));
                throw new NotImplementedException();
            }
        }

        // Restituisce il complesso coniugato del numero complesso (https://en.wikipedia.org/wiki/Complex_conjugate)
        public ComplexNum Conjugate
        {
            get
            {
                return new ComplexNum(Re, -Im);
                throw new NotImplementedException();
            }
        }

        public static ComplexNum operator +(ComplexNum a, ComplexNum b)
        {
            return new ComplexNum(a.Re + b.Re, a.Im + b.Im);
        }
        public static ComplexNum operator -(ComplexNum a, ComplexNum b)
        {
            return new ComplexNum(a.Re - b.Re, a.Im - b.Im);
        }
        public static ComplexNum operator *(ComplexNum a, ComplexNum b)
        {
            return new ComplexNum((a.Re * b.Re) - (a.Im * b.Im),(a.Re * b.Im) + (a.Im * b.Re));
        }
        public static ComplexNum operator /(ComplexNum a, ComplexNum b)
        {
            return new ComplexNum(((a.Re * b.Re) - (a.Im * b.Im)) / ((b.Re * b.Re) + ((b.Im * b.Im))), ((a.Im * b.Re) - (a.Re - b.Im)) / ((b.Re * b.Re) + ((b.Im * b.Im))));
        }

        public ComplexNum Invert()
        {
            ComplexNum c = new ComplexNum(0,0);
            c.Re = Re / (Re * Re + Im * Im);
            c.Im = -(Im / (Re * Re + Im * Im));
            return c;
        }

        // Restituisce una rappresentazione idonea per il numero complesso
        public override string ToString()
        {
            if (Im >= 0)
            {
                return "Numero: " + Re + "+" + Im + "i";
            }
            return "Numero: " + Re + Im + "i";
            throw new NotImplementedException();
        }
    }
}
