﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
    class FrenchDeck
    {

        private Card[] cards;

        public FrenchDeck()
        {
            cards = new Card[54];
        }
        
        public Card this[Enum a, Enum b]
        {
            get
            {
                int i = 0;
                foreach (Card c in cards)
                {
                    if (cards[i].Seed == a.ToString() && cards[i].Value == b.ToString())
                    {
                        break;
                    }
                    i++;
                }
                return cards[i];
            }
        }

        public Card this[Enum a]
        {
            get
            {
                int i = 0;
                foreach (Card c in cards)
                {
                    if (cards[i].Value == a.ToString())
                    {
                        break;
                    }
                    i++;
                }
                return cards[i];
            }
        }

        public void Initialize()
        {
            /*
             * == README ==
             * 
             * I due enumerati (ItalianSeed e ItalianValue) definiti in fondo a questo file rappresentano rispettivamente semi e valori
             * di un tipo mazzo di carte da gioco italiano.
             * 
             * Questo metodo deve inizializzare il mazzo (l'array cards) considerando tutte le combinazioni possibili.
             * 
             * Nota - Dato un generico enumerato MyEnum:
             *   a) è possibile ottenere il numero dei valori presenti con Enum.GetNames(typeof(MyEnum)).Length
             *   b) è possibile ottenere l'elenco dei valori presenti con (MyEnum[])Enum.GetValues(typeof(MyEnum))
             * 
             */
            var s = (FrenchSeed[])Enum.GetValues(typeof(FrenchSeed));
            var v = (FrenchValue[])Enum.GetValues(typeof(FrenchValue));
            int h = 0;
            int i = 0;
            int j = 0;
            for (i = 0; i < Enum.GetNames(typeof(FrenchSeed)).Length; i++)
            {
                for (j = 0; j < Enum.GetNames(typeof(FrenchValue)).Length - 1; j++)
                {
                    cards[h] = new Card(v[j].ToString(), s[i].ToString());
                    h++;
                }
            }
            for (int k = 0; k < 2; k++)
            {
                cards[h] = new Card(v[j].ToString());
                h++;
            }
            //throw new NotImplementedException();
        }

        public void Print()
        {
            /*
             * == README  ==
             * 
             * Questo metodo stampa tutte le carte presenti nel mazzo, con una propria rappresentazione a scelta.
             */
            for (int i = 0; i < cards.Length; i++)
            {
                try
                {
                    Console.WriteLine("Seme: " + cards[i].Seed + " Valore: " + cards[i].Value);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Valore: " + cards[i].Value);
                }
            }
            //throw new NotImplementedException();
        }

    }

    enum FrenchSeed
    {
        PICCHE,
        QUADRI,
        FIORI,
        CUORI
    }

    enum FrenchValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        OTTO,
        NOVE,
        DIECI,
        JACK,
        REGINA,
        RE,
        JOKER
    }
}