﻿using System;

namespace Cards
{
    class Deck
    {
        private Card[] cards;

        public Deck()
        {
            cards = new Card[40];
        }

        public Card this[Enum a, Enum b]
        {
            get
            {
                int i = 0;
                foreach (Card c in cards)
                {
                    if (cards[i].Seed == a.ToString() && cards[i].Value == b.ToString())
                    {
                        break;
                    }
                    i++;
                }
                return cards[i];
            }
        }

        public void Initialize()
        {
            /*
             * == README ==
             * 
             * I due enumerati (ItalianSeed e ItalianValue) definiti in fondo a questo file rappresentano rispettivamente semi e valori
             * di un tipo mazzo di carte da gioco italiano.
             * 
             * Questo metodo deve inizializzare il mazzo (l'array cards) considerando tutte le combinazioni possibili.
             * 
             * Nota - Dato un generico enumerato MyEnum:
             *   a) è possibile ottenere il numero dei valori presenti con Enum.GetNames(typeof(MyEnum)).Length
             *   b) è possibile ottenere l'elenco dei valori presenti con (MyEnum[])Enum.GetValues(typeof(MyEnum))
             * 
             */
            var s = (ItalianSeed[])Enum.GetValues(typeof(ItalianSeed));
            var v = (ItalianValue[])Enum.GetValues(typeof(ItalianValue));
            int h = 0;
            for (int i = 0; i < Enum.GetNames(typeof(ItalianSeed)).Length; i++)
            {
               for (int j = 0; j < Enum.GetNames(typeof(ItalianValue)).Length; j++)
               {
                   cards[h] = new Card(v[j].ToString() , s[i].ToString());
                   h++;
               }
           }
           //throw new NotImplementedException();
        }

        public void Print()
        {
            /*
             * == README  ==
             * 
             * Questo metodo stampa tutte le carte presenti nel mazzo, con una propria rappresentazione a scelta.
             */
            for (int i = 0; i < cards.Length; i++)
            {
                Console.WriteLine("Seme: " + cards[i].Seed + " Valore: " + cards[i].Value);
            }
            //throw new NotImplementedException();
        }

    }

    enum ItalianSeed
    {
        DENARI,
        COPPE,
        SPADE,
        BASTONI
    }

    enum ItalianValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        FANTE,
        CAVALLO,
        RE
    }
}
